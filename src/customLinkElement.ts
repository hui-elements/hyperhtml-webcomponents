export class CustomLinkElement extends HTMLLinkElement {

  public connectedCallback() {
    const url = this.getAttribute("href") + "";
    fetch(url).then((response) => {
      response.text().then((t) => {
        const s = document.createElement("style");
        s.textContent = t;
        s.setAttribute("id", "CustomStyle" + Math.floor(Math.random() * 999999) + 1);
        (window as any).ShadyCSS.CustomStyleInterface.addCustomStyle(s);
        (window as any).ShadyCSS.styleSubtree(this);
        const supportReplaceCssSync = (document as any).adoptedStyleSheets;
        if (supportReplaceCssSync) {
          const styleSheet = new CSSStyleSheet();
          (styleSheet as any).replaceSync(s.textContent);
          (document as any).adoptedStyleSheets = [...(document as any).adoptedStyleSheets, styleSheet];
        } else {
          const blob = new Blob([s.textContent], { type: "text/css" });
          const link = document.createElement("link");
          link.rel = "stylesheet";
          link.href = URL.createObjectURL(blob);
          document.getElementsByTagName("head")[0].appendChild(link);
        }
        this.dispatchEvent(new CustomEvent("customStyleConnected", {
          bubbles: true,
          cancelable: false,
          detail: {
            customStyle: s,
          },
        }));
      });
    });
  }

}
